USE [master]
GO

/****** Object:  Database [BankDB]    Script Date: 11/08/2019 21:47:12 ******/
CREATE DATABASE [BankDB]


USE [BankDB]
GO
/****** Object:  Table [dbo].[ContaCorrente]    Script Date: 11/08/2019 21:46:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ContaCorrente](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[NumeroDobanco] [varchar](10) NULL,
	[NomeDoCorrentista] [varchar](20) NULL,
	[Agencia] [varchar](6) NULL,
	[ContaComDigito] [varchar](12) NULL,
	[Saldo] [decimal](18, 0) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lancamento]    Script Date: 11/08/2019 21:46:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lancamento](
	[IDTransacao] [uniqueidentifier] NOT NULL,
	[ContaCorrenteorigemId] [int] NULL,
	[ContaCorrentedestinoId] [int] NULL,
	[ValorDaTransacao] [decimal](18, 0) NULL,
	[DataDaTransacao] [datetime] NULL,
 CONSTRAINT [PK_ID] PRIMARY KEY CLUSTERED 
(
	[IDTransacao] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[ContaCorrente] ON 

INSERT [dbo].[ContaCorrente] ([ID], [NumeroDobanco], [NomeDoCorrentista], [Agencia], [ContaComDigito], [Saldo]) VALUES (1, N'234', N'José Lima', N'1324', N'234566', CAST(800 AS Decimal(18, 0)))
INSERT [dbo].[ContaCorrente] ([ID], [NumeroDobanco], [NomeDoCorrentista], [Agencia], [ContaComDigito], [Saldo]) VALUES (2, N'341', N'João Lima', N'0034', N'098564', CAST(900 AS Decimal(18, 0)))
SET IDENTITY_INSERT [dbo].[ContaCorrente] OFF
INSERT [dbo].[Lancamento] ([IDTransacao], [ContaCorrenteorigemId], [ContaCorrentedestinoId], [ValorDaTransacao], [DataDaTransacao]) VALUES (N'5aa4d258-785b-464b-944b-41fe913bb11d', 1, 2, CAST(200 AS Decimal(18, 0)), CAST(N'2019-08-11T23:50:45.470' AS DateTime))
