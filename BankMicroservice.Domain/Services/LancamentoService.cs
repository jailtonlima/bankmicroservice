﻿using BankMicroservice.Data.Models;
using BankMicroservice.Data.Interfaces;
using BankMicroservice.Data.Models;
using BankMicroservice.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BankMicroservice.Domain.Services
{
	public class LancamentoService : ILancamentoService
	{
		private readonly ILancamentoRepository _lancamentoRepository;
		private readonly IContaCorrenteService _contaCorrenteService;
		public LancamentoService(ILancamentoRepository lancamentoRepository, IContaCorrenteService contaCorrenteService)
		{
			_contaCorrenteService = contaCorrenteService;
			_lancamentoRepository = lancamentoRepository;
		}
		public bool ExecutarTransferencia(DadosDoLancamento dadosDoLancamento)
		{
			var retornoValidacao = _contaCorrenteService.ValidarTransferenciaParaDebito(dadosDoLancamento);
			if (retornoValidacao)
			{
				_contaCorrenteService.Debitar(dadosDoLancamento.ContaCorrenteorigem, dadosDoLancamento.ValorDaTransacao);
				_contaCorrenteService.Creditar(dadosDoLancamento.ContaCorrentedestino, dadosDoLancamento.ValorDaTransacao);
				var lancamento = new Lancamento() { IDTransacao = dadosDoLancamento.IDTransacao, ContaCorrenteorigemId = dadosDoLancamento.ContaCorrenteorigem.Id, ContaCorrentedestinoId = dadosDoLancamento.ContaCorrentedestino.Id, DataDaTransacao = dadosDoLancamento.DataDaTransacao, ValorDaTransacao = dadosDoLancamento.ValorDaTransacao };


				return _lancamentoRepository.Gravar(lancamento);
			}

			return false;
			
		}
		
	}
}
