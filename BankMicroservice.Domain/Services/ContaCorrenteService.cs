﻿using BankMicroservice.Data.Models;
using BankMicroservice.Data.Interfaces;
using BankMicroservice.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BankMicroservice.Domain.Services
{
	public class ContaCorrenteService : IContaCorrenteService
	{
		private readonly IContaCorrenteRepository _contaCorrenteRepository;
		public ContaCorrenteService(IContaCorrenteRepository contaCorrenteRepository)
		{
			_contaCorrenteRepository = contaCorrenteRepository;
		}
		public ContaCorrente BuscarporCodigo(int id)
		{
			return _contaCorrenteRepository.BuscarPorCodigo(id);
		}

		private decimal ConsultarSaldo(int id)
		{
			var contaCorrente = _contaCorrenteRepository.BuscarPorCodigo(id);
			return contaCorrente.Saldo;
		}

		public void Creditar(ContaCorrente contaCorrente, decimal valor)
		{
			var valorAdicionado = Decimal.Add(contaCorrente.Saldo, Decimal.Floor(valor));
			contaCorrente.Saldo = valorAdicionado;
			_contaCorrenteRepository.Atualizar(contaCorrente);
		}

		public void Debitar(ContaCorrente contaCorrente, decimal valor)
		{
			var valorSubtraido = Decimal.Subtract(contaCorrente.Saldo, Decimal.Floor(valor));
			contaCorrente.Saldo = valorSubtraido;
			_contaCorrenteRepository.Atualizar(contaCorrente);
		}

		public bool ValidarTransferenciaParaDebito(DadosDoLancamento dadosDoLancamento)
		{
			return dadosDoLancamento.ContaCorrenteorigem.Saldo > dadosDoLancamento.ValorDaTransacao;
		}

		public void Gravar(ContaCorrente contaCorrente)
		{
			_contaCorrenteRepository.Gravar(contaCorrente);
		}

		public IList<ContaCorrente> Listar()
		{
			return _contaCorrenteRepository.Listar();
		}
	}
}
