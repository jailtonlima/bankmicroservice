﻿using BankMicroservice.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BankMicroservice.Domain.Interfaces.Services
{
	public interface ILancamentoService
	{
		bool ExecutarTransferencia(DadosDoLancamento lancamento);
	}
}
