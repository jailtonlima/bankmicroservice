﻿using BankMicroservice.Data.Models;
using System.Collections.Generic;

namespace BankMicroservice.Domain.Interfaces.Services
{
	public interface IContaCorrenteService
	{
		ContaCorrente BuscarporCodigo(int id);
		void Debitar(ContaCorrente contaCorrente, decimal valor);
		void Creditar(ContaCorrente contaCorrente, decimal valor);
		bool ValidarTransferenciaParaDebito(DadosDoLancamento dadosDoLancamento);
		void Gravar(ContaCorrente contaCorrente);
		IList<ContaCorrente> Listar();

	}
}
