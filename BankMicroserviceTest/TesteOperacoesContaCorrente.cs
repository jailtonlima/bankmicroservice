﻿using BankMicroservice.Data.Models;
using BankMicroservice.Data.Interfaces;
using BankMicroservice.Domain.Interfaces.Services;
using BankMicroservice.Domain.Services;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace BankMicroserviceTest
{
	
		public class TesteOperacoesContaCorrente
		{


			[Fact]
			public void ValidarTransferenciaParaDebitoSemSaldoSuficiente()
			{

					var contaCorrenteRepository = new Mock<IContaCorrenteRepository>();
					var contaCorrenteService = new ContaCorrenteService(contaCorrenteRepository.Object);

			var dadosDoLancamento = new DadosDoLancamento()
			{
				ContaCorrenteorigem = new ContaCorrente()
				{
					Id = 1,
					Agencia = "0001",
					ContaComDigito = "00001238",
					NumeroDobanco = "345",
					NomeDoCorrentista = "José Lima",
					Saldo = 199
				},

				ValorDaTransacao = 201

			};

			var retorno = contaCorrenteService.ValidarTransferenciaParaDebito(dadosDoLancamento);

					Assert.False(retorno, "Saldo insuficiente");

			}

			[Fact]
			public void ValidarTransferenciaParaDebitoComSaldoSuficiente()
			{

				var contaCorrenteRepository = new Mock<IContaCorrenteRepository>();
				var contaCorrenteService = new ContaCorrenteService(contaCorrenteRepository.Object);

				var dadosDoLancamento = new DadosDoLancamento() {
					 ContaCorrenteorigem = new ContaCorrente()
					 {
						 Id = 1,
						 Agencia = "0001",
						 ContaComDigito = "00001238",
						 NumeroDobanco = "345",
						 NomeDoCorrentista = "José Lima",
						 Saldo = 201
					 },

					 ValorDaTransacao = 201

				};
				

				var retorno = contaCorrenteService.ValidarTransferenciaParaDebito(dadosDoLancamento);

				Assert.True(retorno, "Válido para transferência");

			}


			[Fact]
			public void RealizarTransferenciaParaDebitoComSaldoSuficiente()
			{
				var contaCorrenteRepository = new Mock<IContaCorrenteRepository>();
				var contaCorrenteService = new ContaCorrenteService(contaCorrenteRepository.Object);

				var lancamentoRepository = new Mock<ILancamentoRepository>();
				var lancamentoService = new LancamentoService(lancamentoRepository.Object, contaCorrenteService);
				lancamentoRepository.Setup(x => x.Gravar(It.IsAny<Lancamento>())).Returns(true);

				var contaCorrenteOrigem = new ContaCorrente() { Id = 1, Agencia = "0001", ContaComDigito = "00001238", NumeroDobanco = "345", NomeDoCorrentista = "José Lima", Saldo = 200};
				var contaCorrenteDestino = new ContaCorrente() { Id = 2, Agencia = "0001", ContaComDigito = "00001239", NumeroDobanco = "345", NomeDoCorrentista = "João Lima", Saldo = 50 };

				var lancamento = new DadosDoLancamento() { ContaCorrenteorigem = contaCorrenteOrigem, ContaCorrentedestino = contaCorrenteDestino, DataDaTransacao = new DateTime(), ValorDaTransacao = 100 };
				var resultadoGravacaoLancamento = lancamentoService.ExecutarTransferencia(lancamento);

				Assert.True(resultadoGravacaoLancamento, "Transferência efetuada com sucesso!");

			}

			[Fact]
			public void RealizarTransferenciaParaDebitoSemSaldoSuficiente()
			{
				var contaCorrenteRepository = new Mock<IContaCorrenteRepository>();
				var contaCorrenteService = new ContaCorrenteService(contaCorrenteRepository.Object);

				var lancamentoRepository = new Mock<ILancamentoRepository>();
				var lancamentoService = new LancamentoService(lancamentoRepository.Object, contaCorrenteService);
				lancamentoRepository.Setup(x => x.Gravar(It.IsAny<Lancamento>())).Returns(true);

				var contaCorrenteOrigem = new ContaCorrente() { Id = 1, Agencia = "0001", ContaComDigito = "00001238", NumeroDobanco = "345", NomeDoCorrentista = "José Lima", Saldo = 40 };
				var contaCorrenteDestino = new ContaCorrente() { Id = 2, Agencia = "0001", ContaComDigito = "00001239", NumeroDobanco = "345", NomeDoCorrentista = "João Lima", Saldo = 50 };

				var lancamento = new DadosDoLancamento() { ContaCorrenteorigem = contaCorrenteOrigem, ContaCorrentedestino = contaCorrenteDestino, DataDaTransacao = new DateTime(), ValorDaTransacao = 100 };
				var resultadoGravacaoLancamento = lancamentoService.ExecutarTransferencia(lancamento);

				Assert.False(resultadoGravacaoLancamento, "Transferência não realizada. Saldo insuficiente!");

			}

	}
}
