﻿using BankMicroservice.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BankMicroservice.Data.Interfaces
{
	public interface IContaCorrenteRepository
	{
		ContaCorrente BuscarPorCodigo(int id);
		
		void Gravar(ContaCorrente contaCorrente);

		void Atualizar(ContaCorrente contaCorrente);

		IList<ContaCorrente> Listar();
		
	}
}
