﻿using BankMicroservice.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BankMicroservice.Data.Interfaces
{
	public interface ILancamentoRepository
	{
		bool Gravar(Lancamento lancamento);
	}
}
