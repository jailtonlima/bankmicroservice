﻿using BankMicroservice.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace BankMicroservice
{
	public class BankContext : DbContext
	{
		public DbSet<ContaCorrente> ContaCorrente { get; set; }
		public DbSet<Lancamento> Lancamento { get; set; }
		
		public BankContext(DbContextOptions<BankContext> options)
		   : base(options)
		{
		}

		

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<ContaCorrente>()
				.HasKey(c => c.Id);

			modelBuilder.Entity<Lancamento>()
				.HasKey(c => c.IDTransacao);

		}

	}
}
