﻿using BankDDD.Data.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankMicroservice.Data.Models
{
	public class ContaCorrente : BaseEntity
	{
		public string NumeroDobanco { get; set; }
		public string NomeDoCorrentista { get; set; }
		public string Agencia { get; set; }
		public string ContaComDigito { get; set; }
		public decimal Saldo { get; set; }

	}
}
