﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BankMicroservice.Data.Models
{
	
	public class Lancamento
	{
		public Guid IDTransacao { get; set; }
		public int ContaCorrenteorigemId { get; set; }
		public int ContaCorrentedestinoId { get; set; }
		public decimal ValorDaTransacao { get; set; }
		public DateTime DataDaTransacao { get; set; }

	}
}
