﻿using BankDDD.Data.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankMicroservice.Data.Models
{
	public class DadosDoLancamento 
	{
		public Guid IDTransacao  { get; set; }
		public ContaCorrente ContaCorrenteorigem { get; set; }
		public ContaCorrente ContaCorrentedestino { get; set; }
		public decimal ValorDaTransacao { get; set; }
		public DateTime DataDaTransacao { get; set; }

	}
}
