﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankDDD.Data.Abstractions
{
	public class BaseEntity
	{
		public virtual int Id { get; set; }
	}
}
