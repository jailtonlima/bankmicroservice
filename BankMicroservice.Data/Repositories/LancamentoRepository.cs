﻿using BankMicroservice.Data.Interfaces;
using BankMicroservice.Data.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BankMicroservice.Data.Repositories
{
	public class LancamentoRepository : ILancamentoRepository
	{

		private readonly BankContext _bankContext;
		public LancamentoRepository(BankContext bankContext)
		{
			_bankContext = bankContext;
		}
		public bool Gravar(Lancamento lancamento)
		{
			
				_bankContext.Lancamento.Add(lancamento);
				return _bankContext.SaveChanges() > 0;
			
		}
	}
}
