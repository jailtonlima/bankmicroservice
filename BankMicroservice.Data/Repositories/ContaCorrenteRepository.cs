﻿using BankDDD.Data.Abstractions;
using BankMicroservice.Data.Models;
using BankMicroservice.Data.Interfaces;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BankMicroservice.Data.Repositories
{
	public class ContaCorrenteRepository : IContaCorrenteRepository
	{
		private readonly BankContext _bankContext;
		public ContaCorrenteRepository(BankContext bankContext)
		{
			_bankContext = bankContext;
		}
		public ContaCorrente BuscarPorCodigo(int id)
		{
			
				return _bankContext.ContaCorrente
					.FirstOrDefault(b => b.Id == id);
			
		}

		public void Gravar(ContaCorrente contaCorrente)
		{

			_bankContext.ContaCorrente.Add(contaCorrente);
			_bankContext.SaveChanges();
			
		}

		public void Atualizar(ContaCorrente contaCorrente)
		{

			_bankContext.ContaCorrente.Update(contaCorrente);
			_bankContext.SaveChanges();

		}

		public IList<ContaCorrente> Listar()
		{
			
			return _bankContext.ContaCorrente.ToList();
			
		}

	}
}
