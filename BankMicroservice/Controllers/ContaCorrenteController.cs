﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BankMicroservice.Data.Models;
using BankMicroservice.Domain.Interfaces.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BankMicroservice.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContaCorrenteController : ControllerBase
    {
		private readonly IContaCorrenteService _contaCorrenteService;
		public ContaCorrenteController(IContaCorrenteService contaCorrenteService)
		{
			_contaCorrenteService = contaCorrenteService;
		}

		// GET: api/Lancamento/5
		[HttpGet("{id}", Name = "Get")]
		public ContaCorrente Get(int id)
		{
			return _contaCorrenteService.BuscarporCodigo(id);
		}

		// POST: api/ContaCorrente
		[HttpPost]
        public void Post([FromBody]ContaCorrente contaCorrente)
        {
			_contaCorrenteService.Gravar(contaCorrente);
		}
		
	}
}
