﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BankMicroservice.Data.Models;
using BankMicroservice.Domain.Interfaces.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BankMicroservice.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LancamentoController : ControllerBase
    {

		private readonly ILancamentoService _lancamentoService;
		public LancamentoController(ILancamentoService lancamentoService)
		{
			_lancamentoService = lancamentoService;
		}
        
		// POST: api/Lancamento
		[HttpPost]
		public void Post([FromBody]DadosDoLancamento lancamento)
		{
			lancamento.IDTransacao = Guid.NewGuid();
			_lancamentoService.ExecutarTransferencia(lancamento);
		}

    }
}
