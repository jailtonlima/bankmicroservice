﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BankMicroservice.Data;
using BankMicroservice.Data.Interfaces;
using BankMicroservice.Data.Repositories;
using BankMicroservice.Domain.Interfaces.Services;
using BankMicroservice.Domain.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.Swagger;

namespace BankMicroservice
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

			services.AddScoped<IContaCorrenteService, ContaCorrenteService>();
			services.AddScoped<IContaCorrenteRepository, ContaCorrenteRepository>();

			services.AddScoped<ILancamentoService, LancamentoService>();
			services.AddScoped<ILancamentoRepository, LancamentoRepository>();

			services.AddEntityFrameworkSqlServer()
				.AddDbContext<BankContext>(
					options => options.UseSqlServer(
						Configuration.GetConnectionString("BaseBank")));

			services.AddSwaggerGen(c =>
			{
				c.SwaggerDoc("v1", new Info { Title = "Bank Api Microservice", Version = "v1" });
			});


		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
			else
			{
				// The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
				app.UseHsts();
			}
			
			app.UseHttpsRedirection();
			app.UseMvc();

			app.UseSwagger();
			app.UseSwaggerUI(c =>
			{
				c.SwaggerEndpoint("/swagger/v1/swagger.json", "Bank Api V1");
			});





		}
	}
}
